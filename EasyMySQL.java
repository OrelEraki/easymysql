package com.net.easymysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 * <h3>EasyMySQL</h3>
 * Easy to use MySQL lightweight database access utility class facade.
 * 
 * @version 1.03
 * @author Orel Eraki [<a href="mailto:orel.eraki@gmail.com">OrelEraki@gmail.com</a>]
 * @see <a href="http://orelsoft.info">http://orelsoft.info</a>
 */
public class EasyMySQL
{
	
	// Server main settings
	private String dbServer = null;
	private String dbUsername = null;
	private String dbPassword = null;
	private String dbDatabase = null;
	private int dbPort;
	
	// Server variables.
	private Connection serverConnection = null;
	private Statement serverStatement = null;
	
	// Log variables.
	private String lastQuery = null;
	private ResultSet lastResultSet = null;
	private SQLException lastErrorMessage = null;
	
	/**
	 * Constructs EasyMySQL object with full parameters.
	 * @param server
	 * @param port
	 * @param username
	 * @param password
	 * @param database
	 */
	public EasyMySQL(String server, int port, String username, String password, String database)
	{
		dbServer = server;
		dbPort = port;
		dbUsername = username;
		dbPassword = password;
		dbDatabase = database;
	}
	
	/**
	 * Constructs EasyMySQL object.
	 * @param server
	 * @param port
	 * @param username
	 * @param password
	 */
	public EasyMySQL(String server, int port, String username, String password)
	{
		this(server, port, username, password, "");
	}
	
	/**
	 * Retrieves whether this Statement object has been closed.
	 * @return true if this Statement object is closed; false if it is still open.
	 */
	public boolean isClosed()
	{
		return serverStatement == null;
	}
	
	/**
	 * Retrieves whether this Statement object is open..
	 * @return true if this Statement object is open; false if it is still closed.
	 */
	public boolean isConnected()
	{
		return !isClosed();
	}
	
	/**
	 * Clearing server log variables.
	 */
	private void clear()
	{
		lastQuery = null;
		lastErrorMessage = null;
		closeResultSet();
	}
	
	/**
	 * Retrieves the Query string from the last command performed.
	 * @return
	 */
	public String getLastQuery()
	{
		return lastQuery;
	}
	
	/**
	 * Retrieves the ResultSet object from the last command performed.
	 * @return
	 */
	public ResultSet getLastResultSet()
	{
		return lastResultSet;
	}
	
	/**
	 * Retrieves the SQLException object from the last command performed.
	 * @return
	 */
	public SQLException getLastErrorMessage()
	{
		return lastErrorMessage;
	}
	
	/**
	 * Connects to MySQL server.
	 * @return true if this Statement object is open; false if it is still closed.
	 * @throws SQLException
	 */
	public boolean connect() throws SQLException
	{
		serverConnection = DriverManager.getConnection(String.format("jdbc:mysql://%s:%d/%s", dbServer, dbPort, dbDatabase), dbUsername, dbPassword);
		serverStatement = serverConnection.createStatement();
		return true;
	}
	
	/**
	 * Releases this Connection, Statement and ResultSet object's database and JDBC resources immediately instead of waiting for them to be automatically released.
	 */
	public void close()
	{
		closeResultSet();
		closeStatement();
		closeConnection();
	}
	
	/**
	 * Cancels this Statement object if both the DBMS and driver support aborting an SQL statement. This method can be used by one thread to cancel a statement that is being executed by another thread.
	 */
	public void cancel()
	{
		try
		{
			serverStatement.close();
		}
		catch (SQLException e)
		{
			lastErrorMessage = e;
		}
	}
	
	/**
	 * Retrieves the number of seconds the driver will wait for a Statement object to execute. If the limit is exceeded, a SQLException is thrown.
	 * @return the current query timeout limit in seconds; zero means there is no limit.
	 */
	public int getQueryTimeout()
	{
		try
		{
			return serverStatement.getQueryTimeout();
		}
		catch (SQLException e)
		{
			e = lastErrorMessage;
		}
		
		return -1;
	}
	
	/**
	 * Sets the number of seconds the driver will wait for a Statement object to execute to the given number of seconds. If the limit is exceeded, an SQLException is thrown. A JDBC driver must apply this limit to the execute, executeQuery and executeUpdate methods. JDBC driver implementations may also apply this limit to ResultSet methods (consult your driver vendor documentation for details).
	 * @param seconds the new query timeout limit in seconds; zero means there is no limit.
	 */
	public void setQueryTimeout(int seconds)
	{
		try
		{
			serverStatement.setQueryTimeout(seconds);
		}
		catch (SQLException e)
		{
			lastErrorMessage = e;
		}
	}

	/**
	 * Executes the given SQL statement, which may be an INSERT, UPDATE, or DELETE statement or an SQL statement that returns nothing, such as an SQL DDL statement.
	 * @param sql an SQL Data Manipulation Language (DML) statement, such as INSERT, UPDATE or DELETE; or an SQL statement that returns nothing, such as a DDL statement.
	 * @return either (1) the row count for SQL Data Manipulation Language (DML) statements or (2) 0 for SQL statements that return nothing
	 */
	public int executeUpdate(String sql)
	{
		clear();
		int result = -1;
		try
		{
			lastQuery = sql;
			result = serverStatement.executeUpdate(sql);
		}
		catch (SQLException e)
		{
			lastErrorMessage = e;
		}
		
		return result;
	}
	
	/**
	 * Executes the given SQL statement, which returns a single ResultSet object.
	 * @param sql an SQL statement to be sent to the database, typically a static SQL SELECT statement
	 * @return a ResultSet object that contains the data produced by the given query; never null
	 */
	public ResultSet executeQuery(String sql)
	{
		clear();
		try
		{
			lastQuery = sql;
			lastResultSet = serverStatement.executeQuery(sql);
		}
		catch (SQLException e)
		{
			lastErrorMessage = e;
		}
		
		return lastResultSet;
	}
	
	/**
	 * Releases this ResultSet object's database and JDBC resources immediately instead of waiting for this to happen when it is automatically closed.
	 */
	private void closeResultSet()
	{
		if (lastResultSet !=null)
		{
			try
			{
				lastResultSet.close();
			}
			catch (SQLException e)
			{
				lastErrorMessage = e;
				//System.out.println("The server ResultSet cannot be closed:" + e);
			}
		}
	}
	
	/**
	 * Releases this Statement object's database and JDBC resources immediately instead of waiting for this to happen when it is automatically closed.
	 */
	private void closeStatement()
	{
		clear();
		if (serverStatement != null)
		{
			try
			{
				serverStatement.close();
			}
			catch (SQLException e)
			{
				lastErrorMessage = e;
				//System.out.println("The server Statement cannot be closed:" + e);
			}
		}	
	}
	
	/**
	 * Releases this Connection object's database and JDBC resources immediately instead of waiting for this to happen when it is automatically closed.
	 */
	private void closeConnection()
	{
		clear();
		if (serverConnection != null)
		{
			try
			{
				serverConnection.close();
			}
			catch (SQLException e)
			{
				lastErrorMessage = e;
				//System.err.println("The server Connection cannot be closed:" + e);
			}
		}
	}
}
